package net.rgielen.spring.basics.demobeans;

import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Component
public class DummyDao {
    public Object get(Long id) {
        return null;
    }
}
