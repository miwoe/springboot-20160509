package net.rgielen.spring.basics;

import net.rgielen.spring.basics.demobeans.Bar;
import net.rgielen.spring.basics.demobeans.Foo;
import net.rgielen.spring.basics.demobeans.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Configuration
@ComponentScan(basePackageClasses = GenericService.class)
public class SpringBasicsConfig {

    @Bean
    public Bar myBar() {
        return new Bar();
    }

    @Bean @Autowired
    public Foo myFoo(Bar bar) {
        final Foo foo = new Foo();
        foo.setBar(bar);
        return foo;
    }

}
