package net.rgielen.spring.basics;

import net.rgielen.spring.basics.demobeans.Foo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class SpringDemo {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Foo bean = context.getBean(Foo.class);
        System.out.println(bean);
        context.close();
    }
}
