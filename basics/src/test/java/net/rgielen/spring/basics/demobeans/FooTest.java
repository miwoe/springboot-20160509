package net.rgielen.spring.basics.demobeans;

import net.rgielen.spring.basics.SpringBasicsConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringBasicsConfig.class)
public class FooTest {

    @Autowired
    private Foo foo;

    @Autowired
    Bar bar;

    @Autowired
    TutNixService tutNixService;

    @Test
    public void testFooGetsBuildBySpring() throws Exception {
        assertNotNull(foo);
    }

    @Test
    public void testFooHasBarReferenceAfterInjection() throws Exception {
        assertNotNull(foo.bar);
    }

    @Test
    public void testBarGetsManaged() throws Exception {
        assertNotNull(bar);
    }

    @Test
    public void testComponentGetsScannedAndManaged() throws Exception {
        assertNotNull(tutNixService);
    }

}