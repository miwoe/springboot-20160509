package net.rgielen.spring.basics.demobeans;

import net.rgielen.spring.basics.SpringBasicsConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringBasicsConfig.class)
public class ScopeTest {

    @Autowired
    DummyService dummyService1;
    @Autowired
    DummyService dummyService2;

    @Autowired
    TutNixService tutNixService1;
    @Autowired
    TutNixService tutNixService2;

    @Autowired
    ApplicationContext context;

    @Test
    public void testThatAlwaysTheSameSingletonIsInjected() throws Exception {
        assertSame(dummyService1, dummyService2);
    }

    @Test
    public void testThatNeverTheSamePrototypeIsInjected() throws Exception {
        assertNotSame(tutNixService1, tutNixService2);
    }

    @Test
    public void testRequestedPrototypesAreNotSame() throws Exception {
        assertNotSame(context.getBean(TutNixService.class), context.getBean(TutNixService.class));
    }

    @Test
    public void testRequestedSingletonsAreSame() throws Exception {
        assertSame(context.getBean(DummyService.class), context.getBean(DummyService.class));
    }
}
